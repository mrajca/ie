<?php
App::uses('Attraction', 'Model');

/**
 * Attraction Test Case
 *
 */
class AttractionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.attraction',
		'app.country',
		'app.city'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Attraction = ClassRegistry::init('Attraction');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Attraction);

		parent::tearDown();
	}

}
