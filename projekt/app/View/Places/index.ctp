<div class="places index">
	<div class="row">
               <div class="col-lg-4">

			</div></div>
	<h2><?php echo __('Lista Atrakcji'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('country_id', 'Kraj'); ?></th>
			<th><?php echo $this->Paginator->sort('city', 'Miasto'); ?></th>
			<th><?php echo $this->Paginator->sort('name', 'Nazwa atrakcji'); ?></th>
			<th><?php echo $this->Paginator->sort('description', 'Opis'); ?></th>
			<th><?php echo $this->Paginator->sort('created', 'Dodano'); ?></th>
			<th class="actions"><?php echo __('Akcje'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($places as $place): ?>
	<tr>
	<td><?php echo ($place['Country']['name']); ?></td>
				<td><?php echo h($place['Place']['city']); ?>&nbsp;</td>
		<td><?php echo h($place['Place']['name']); ?>&nbsp;</td>
		<td><?php 
		
			echo substr(($place['Place']['description']), 0, 70);
			echo "...";
		
		?>&nbsp;</td>

		<td><?php echo h($place['Place']['created']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link("<span style='color:#337AB7' class='glyphicon glyphicon-eye-open' aria-hidden='true'></span>", array('action' => 'view', $place['Place']['id']),  array('escape' => false)); ?>
			<?php if($current_user['username'] == $place['Place']['author'] || $current_user['role'] == 'admin' ):?>
				<?php echo $this->Html->link("<span style='color:#337AB7' class='glyphicon glyphicon-pencil' aria-hidden='true'></span>", array('action' => 'edit', $place['Place']['id']),  array('escape' => false)); ?>
								<?php 
				echo $this->Form->postLink(
						$this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash', 'style'=>'color: red;')),
						array('action' => 'delete', $place['Place']['id']),
						array('escape'=>false),__('na pewno usunąć?', $place['Place']['id']),
						array('class' => 'btn btn-mini')
				);

				?>
				<?php //echo $this->Form->postLink(__('Usuń'), array('action' => 'delete', $place['Place']['id']), array(), __('Are you sure you want to delete # %s?', $place['Place']['id'])); ?>
			<?php endif;?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	<div class="paging">
	<?php
		//echo $this->Paginator->prev('< ' . __('poprzedni '), array(), null, array('class' => 'prev disabled'));
		//echo $this->Paginator->numbers(array('separator' => ''));
		//echo $this->Paginator->next(__(' następny') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Co chcesz zrobić?'); ?></h3>
	<ol>
		<ul><?php echo $this->Html->link(__('Dodaj nową atrakcję'), array('action' => 'add')); ?></li>
	</ol>
</div>
