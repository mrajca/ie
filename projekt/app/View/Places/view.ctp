
<!-- wyświetlanie atrakcji start -->
<div class="places view">
<h2><?php echo __('Atrakcja'); ?></h2>
	<dl> 
			<dt><?php echo __('Autor'); ?></dt>
		<dd>
			<?php echo h($place['Place']['author']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nazwa atrakcji'); ?></dt>
		<dd>
			<?php echo h($place['Place']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Opis'); ?></dt>
		<dd>
			<?php echo h($place['Place']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Kraj'); ?></dt>
		<dd>
			<?php echo ($place['Country']['name'])?>
			&nbsp;
		</dd>
		<dt><?php echo __('Miasto'); ?></dt>
		<dd>
			<?php echo h($place['Place']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zdjęcie'); ?></dt>
		<dd>
			<img class="img-responsive" src=<?php echo h($place['Place']['image']); ?>>
			&nbsp;
		</dd>

<!-- wyświetlanie atrakcji koniec -->

	</dl>
</div>

<div  <?php if($current_user['role'] != 'admin') echo ("style=\"display:none;\"")?>>
	<h3><?php echo __('Akcje admina'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Place'), array('action' => 'edit', $place['Place']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Place'), array('action' => 'delete', $place['Place']['id']), array(), __('Are you sure you want to delete # %s?', $place['Place']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Comments'), array('controller' => 'comments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Comment'), array('controller' => 'comments', 'action' => 'add')); ?> </li>
	</ul>
</div>

<!-- część odpowiadająca za wyświetlanie koentarzy -->

<div class="related">
	<h3><?php echo __('Komentarze Podróżników:'); ?></h3>
	<?php if (!empty($place['Comment'])): ?>
	<table cellpadding = "0" cellspacing = "0"  class="table table-striped">
	<tr>
	
		<th><?php echo __('login'); ?></th>
		<th><?php echo __('treść komentarza'); ?></th>
		<th><?php echo __('dodany'); ?></th>
		<th><?php echo __('ocena'); ?></th>
	</tr>
	<?php foreach ($place['Comment'] as $comment): ?>
		<tr>
			
			<td><?php echo $comment['name']; ?></td>
			<td><?php echo $comment['content']; ?></td>
			<td><?php echo $comment['created']; ?></td>
			<td><?php 
			echo ("<div style='width: 100px;'>");
			//wyświetlanie gwiazdek w komentarzach
			for($i = 0; $i<$comment['rate'];$i++){
			echo ("<span style='color:#F0AD4E' class='glyphicon glyphicon-star' aria-hidden='true'></span>"); //#337AB7
			}
			for($i = 0; $i<5-$comment['rate'];$i++){
				echo ("<span style='color:#337AB7' class='glyphicon glyphicon-star-empty' aria-hidden='true'></span>"); //#337AB7
			}
			echo ("</div>");
			?></td>
			<td class="actions" style="width: 100px;">
			<!-- akcje admina -->
				<?php if($current_user['username'] == $comment['name'] || $current_user['role'] == 'admin' ):?>
				<?php echo $this->Html->link("<span style='color:#337AB7' class='glyphicon glyphicon-pencil' aria-hidden='true'></span>", array('controller' => 'comments', 'action' => 'edit', $comment['id'], $place['Place']['id']), array('escape' => false)); ?>
				<?php //echo $this->Form->postLink("<b>x</b>",  array('escape' => false), array('controller' => 'comments', 'action' => 'delete', $comment['id']), array(), __('Are you sure you want to delete # %s?', $comment['id'])); ?>
				<?php echo ("&nbsp;")?>
				<?php 
				echo $this->Form->postLink(
						$this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-trash', 'style'=>'color: red;')),
						array('controller' => 'comments', 'action' => 'delete', $comment['id'], $place['Place']['id']),
						array('escape'=>false),
						__('na pewno usunąć?', $comment['id']),
						array('class' => 'btn btn-mini')
				);
				endif;
				?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

<!-- dodawanie komentarzy -->


<div  class="form-group">
	<form action="/projekt/comments/add" id="CommentAddForm" method="post" accept-charset="utf-8"><div style="display:none;"><input name="_method" value="POST" type="hidden"></div>	<fieldset>
		<legend>Dodaj komentarz i ocenę</legend>
	<div class="input select required">
	<label for="CommentContent">Ocena</label>
	<select  class="form-control" name="data[Comment][rate]" id="CommentRate" required="required">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
	</select>
						<input name="data[Comment][place_id]" id="CommentPlaceId" required="required" value=<?php echo h($place['Place']['id']); ?> style="visibility: hidden; display: none;">
						<input name="data[Comment][name]" maxlength="255" id="CommentName" required="required" type="text" value=<?php echo $current_user['username']?> style="visibility: hidden; display: none;">
	<label for="Content">Treść</label>
	<textarea  class="form-control"  name="data[Comment][content]" cols="30" rows="6" id="CommentContent" required="required"></textarea></fieldset>
	<br>
	<div class="submit"><input  class="btn btn-primary" value="dodaj" type="submit"></div></form>
</div>

</div>
