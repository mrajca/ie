<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 */

$cakeDescription = __d('cake_dev', 'Atrakcje Świata, czego chcieć więcej');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())

?>



<!DOCTYPE html>
<html>
<head>
    
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	        <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
        <?php
		echo $this->Html->meta(
		    'favicon.ico',
		    '/favicon.ico',
		    array('type' => 'icon')
);

		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
                echo $this->Html->css('bootstrap');

		echo $this->fetch('script');
	?>
    
       
        <script src="../js/bootstrap.js"></script>

</head>
<body>
	<div id="container">
				<div id="header">

                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div id="logo"></div>
                        </div>
                        <div class="hidden-xs col-sm-6 col-md-5 col-lg-6 myMenu">
                            <a class="btn btn-warning" href="/projekt/users/add">Zapisz się</a>
                            <a class="btn btn-link" href="/projekt/places/index">Eksploruj</a>
                            <a class="btn btn-link" href="/projekt/places/add">Dodaj</a>
                        </div>
                    <div class="hidden-xs col-sm-3 col-md-3 col-lg-3 loginLogoutBox" style="color: grey;">

                        <?php if($logged_in):?>
					zalogowany jako <?php echo $current_user['username']?> 
					<?php 
                       echo $this->Html->link('wyloguj ', array('controller'=>'users', 'action'=>'logout')); 
                     ?>
				<?php else: ?>
					<?php 
                       echo $this->Html->link('zaloguj się ', array('controller'=>'users', 'action'=>'login')) 
                      ?>
				<?php endif; ?>
                    </div>

	</div>
		 <div class="container-fluid">
		<div id="content">

			<br />
                        <?php 
                            if(isset($myName)){
                                echo "<div class='roundBox col-lg-8 col-lg-offset-2'>";
                            }else{
                                //echo 'nie jest';
                            }
                        ?>
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Session->flash('auth'); ?>
			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			
		</div>
	</div>
	
        
</body>
</html>
	