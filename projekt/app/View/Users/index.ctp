
	<h2><?php echo __('Podróżnicy'); ?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-hover table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('username','nazwa użytkownika'); ?></th>

			<th><?php echo $this->Paginator->sort('email', 'e-mail'); ?></th>
			<th><?php echo $this->Paginator->sort('name', 'imie i nazwisko'); ?></th>
			<th><?php echo $this->Paginator->sort('rola'); ?></th>
			<th class="actions"><?php echo __('akcje'); ?></th>
	</tr>
	</thead>
        <tbody>

	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['username']); ?>&nbsp;</td>

		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['role']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Zobacz'), array('action' => 'view', $user['User']['id'])); ?>
			<?php if($current_user['id'] == $user['User']['id'] || $current_user['role'] == 'admin' ):?>
				<?php echo $this->Html->link(__('Edytuj'), array('action' => 'edit', $user['User']['id'])); ?>
				<?php echo $this->Form->postLink(__('Usuń'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
			<?php endif; ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	//echo $this->Paginator->counter(array(	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('poprzednia '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__(' następna') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
        <br />

</div>