<div class="mySlider hidden-xs col-sm-offset-1">
<div id="carousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="../slides/s1.JPG" alt="z1">
      <div class="carousel-caption">
        Katmandu - Nepal
      </div>
    </div>
      
    <div class="item">
     <img src="../slides/s2.JPG" alt="z2">
      <div class="carousel-caption">
        Grenlandia
      </div>
    </div>

    <div class="item">
     <img src="../slides/s3.JPG" alt="z2">
      <div class="carousel-caption">
        Hobbiton - Nowa Zelandia
      </div>
    </div>
      
     <div class="item">
     <img src="../slides/s4.JPG" alt="z2">
      <div class="carousel-caption">
        Mount Ewerest
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
    <span style="color: #f7941e;" class="glyphicon glyphicon-chevron-left" ></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
    <span style="color: #1f83c5;" class="glyphicon glyphicon-chevron-right" aria-hidden="true">
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
<div class="col-lg-10 col-lg-offset-1 welcomeText">
    <div class="welcomeTitle">Dziel się wspomnieniami</div>
    <div class="welcomeContent">
        
        Dodawaj miejsca które odwiedziłeś i dziel się nimi z całym światem. Atrakcje Świata to miejsce w którym za darmo możesz gromadzić swoje wspomnienia i pomagać 
        innym wyruszyć w podróż życia. Dodane przez Ciebie atrakcje mogą być oceniane i komentowane przez wyszystkich. Możesz również dodawać wskazówki dla przyszłych podróżników.
    </div>    
</div>
<script>
</script>