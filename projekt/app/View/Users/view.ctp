<div class="users view">
<h2><?php echo __('dane użytkownika: ');
           echo h($user['User']['username']);
    ?></h2>
    <br />
	<dl class="dl-horizontal">
		
		<dt><?php echo __('nazwa'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('imie i nazwisko'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('rola'); ?></dt>
		<dd>
			<?php echo h($user['User']['role']); ?>
			&nbsp;
		</dd>
	</dl>
    <br />
    <?php echo $this->Html->link(__('powrót do listy'), array('action' => 'index')); ?>
    <br /><br />
</div>
