-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Mar 2015, 19:40
-- Wersja serwera: 5.6.20
-- Wersja PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `projekt`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `attractions`
--

CREATE TABLE IF NOT EXISTS `attractions` (
`id` int(11) NOT NULL,
  `name` text COLLATE utf8_polish_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `image_url` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `attractions`
--

INSERT INTO `attractions` (`id`, `name`, `country_id`, `city_id`, `image_url`) VALUES
(1, 'Góra Ararat, super wysoka góra, w której cieniu położony jest Erywań.', 10, 3, 'http://www.armeniawycieczki.com/upload/gallery/yerevan-city.jpg'),
(4, 'Luwr, fajne muzeum.', 52, 2, 'http://static.lovetotravel.pl/galery/th/th1_687_paryz_luwr.jpg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `cities`
--

INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES
(1, 'Warszawa', 136),
(2, 'Paryż', 52),
(3, 'Erywań', 10),
(4, 'Rzym', 187);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`id` int(11) NOT NULL,
  `place_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `created` datetime NOT NULL,
  `rate` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=57 ;

--
-- Zrzut danych tabeli `comments`
--

INSERT INTO `comments` (`id`, `place_id`, `name`, `content`, `created`, `rate`) VALUES
(2, 1, 'xxx', 'xxx', '2015-01-09 22:15:16', 0),
(4, 9, 'qqqqqqqqqq', 'qqqqqqqqq', '2015-01-09 22:36:34', 0),
(5, 9, 'zxczxc', 'czxczxczxczxc', '2015-01-09 22:36:49', 0),
(6, 43, 'zzzzzzzzzzzzzzz', 'zzzzzzzzzzzzzzzzz', '2015-01-09 22:36:59', 0),
(7, 43, 'kuls', 'xxxaaaeee', '2015-01-10 12:02:32', 4),
(8, 43, 'super', 'adasdasdasd1212121212121212121212123123123', '2015-01-10 12:16:32', 3),
(9, 1, 'super', 'eeeeeeeeeeeeeeeeeeeeeeeeeeeewwwwwwwwwwwwwqqqqqqqqqqqqqqqqq', '2015-01-10 12:17:20', 5),
(10, 1, 'super', 'całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry całkiem fajny zabytek i w ogóle dobry ', '2015-01-10 12:48:05', 1),
(11, 1, 'test', 'asda', '2015-01-10 22:04:06', 3),
(12, 1, 'test', 'aaaaaaaaaaa', '2015-01-10 22:05:24', 4),
(13, 1, 'test', 'asdasdasdasd1234234234', '2015-01-10 22:05:43', 4),
(14, 1, 'test', 'kojot', '2015-01-10 22:12:53', 2),
(15, 1, 'test', 'asdasdasd', '2015-01-10 22:14:25', 3),
(16, 1, 'test', 'asdasdasd', '2015-01-10 22:14:58', 3),
(17, 1, 'test', 'asdasdasdgjhghjghj', '2015-01-10 22:15:12', 0),
(18, 1, 'test', 'asdasdasdgjhghjghj', '2015-01-10 22:15:13', 0),
(19, 1, 'asasd', 'asdasdasd', '2015-01-10 22:15:18', 0),
(20, 1, 'asasd', 'asdasdasd', '2015-01-10 22:16:50', 0),
(21, 1, 'sssss', 'ssssssss', '2015-01-10 22:16:59', 0),
(22, 1, 'asd', 'asdasd', '2015-01-10 22:17:22', 0),
(23, 1, 'asd', 'asdasd', '2015-01-10 22:18:32', 0),
(24, 1, 'test', 'asd12123123123', '2015-01-10 22:19:19', 3),
(30, 2, 'test', 'asdasd', '2015-01-10 22:55:25', 3),
(31, 2, 'test', 'adsdasdasdasdasd', '2015-01-10 22:55:46', 4),
(32, 3, 'alinka', 'asdasdasd', '2015-01-10 23:01:00', 3),
(33, 4, 'alinka', 'całkiem fajna atrakcja', '2015-01-10 23:46:57', 4),
(34, 4, 'super', 'leżing, plażing', '2015-01-10 23:49:04', 5),
(35, 5, 'alinka', 'nie byłem stwierdzam że nie warto być...', '2015-01-10 23:54:32', 1),
(36, 5, 'rudy102', '@up kolega alinka chyba z Turunia...', '2015-01-10 23:55:48', 5),
(37, 6, 'rudy102', 'Byłem, widziałem. Okradli mnie tam :(', '2015-01-10 23:59:47', 1),
(38, 7, 'alinka', 'jako Alinka Rajca się zgadzam!', '2015-01-11 00:02:02', 5),
(39, 8, 'rudy102', 'Bez szału. Poza okresem lęgowym nic nie ma do zobaczenia', '2015-01-11 00:03:28', 3),
(40, 8, 'alinka', 'byłam widziałam polecam', '2015-01-11 00:04:07', 5),
(41, 7, 'rudy102', 'Polecam. B. dobrze mnie przyjęli. Dali herbatę i ciastka :))', '2015-01-11 00:04:17', 5),
(42, 8, 'youpendi', 'cudowne miejsce, do którego zawsze będę wracać z ogromną przyjemnością i nutką ekscytacji!\r\nmiejsce, gdzie pierwszy raz poczułam się samodzielna.', '2015-01-11 00:05:22', 5),
(43, 9, 'alinka', 'Odradzam, mój syn Mateuszek się tam rozpija i obżera kebabami.', '2015-01-11 00:09:09', 1),
(44, 9, 'youpendi', 'kim był Karłowicz?', '2015-01-11 00:10:27', 5),
(45, 9, 'rudy102', 'Marka sama w sobie. Polecam z czystym sercem. Spokojnie Alinko- kebab jest nisko tłuszczowy, a wódeczka koszerna\r\n', '2015-01-11 00:14:23', 5),
(46, 2, 'alinka', 'nie wiem ', '2015-01-11 00:18:02', 4),
(47, 10, 'alinka', 'mocne miejsce, zrobili w chwilkę, pomogli innym. gość z Bochni projektował.\r\nMega logo!!!', '2015-01-11 00:26:16', 5),
(48, 8, 'mathjas', 'też byłem dzięki użytkownikowi Youpendi!', '2015-01-11 00:36:21', 5),
(49, 6, 'mathjas', 'nie polecam', '2015-01-11 00:36:47', 1),
(50, 11, 'alinka', 'synu, czego się o Tobie dowiaduje... zdecydowanie źle oceniam!', '2015-01-11 00:45:40', 1),
(51, 2, 'super', 'fajne', '2015-01-18 22:47:31', 3),
(53, 5, 'test', 'asdasdasd', '2015-01-19 11:50:09', 1),
(54, 4, 'rudy102', 'nie stać mnie na wyjazd więc hejt!', '2015-01-29 09:58:56', 1),
(55, 4, 'super', 'aasddasdasd', '2015-02-10 22:22:46', 4),
(56, 6, 'xxx', 'asdasdasd', '2015-02-17 13:17:38', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=196 ;

--
-- Zrzut danych tabeli `countries`
--

INSERT INTO `countries` (`id`, `name`) VALUES
(2, 'Afganistan'),
(3, 'Albania'),
(4, 'Algieria'),
(5, 'Andora'),
(6, 'Angola'),
(7, 'Antigua i Barbuda'),
(8, 'Arabia Saudyjska'),
(9, 'Argentyna'),
(10, 'Armenia'),
(11, 'Australia'),
(12, 'Austria'),
(13, 'Azerbejdżan'),
(14, 'Bahamy'),
(15, 'Bahrajn'),
(16, 'Bangladesz'),
(17, 'Barbados'),
(18, 'Belgia'),
(19, 'Belize'),
(20, 'Benin'),
(21, 'Bhutan'),
(22, 'Białoruś'),
(23, 'Birma'),
(24, 'Boliwia'),
(25, 'Bośnia i Hercegowina'),
(26, 'Botswana'),
(27, 'Brazylia'),
(28, 'Brunei'),
(29, 'Bułgaria'),
(30, 'Burkina Faso'),
(31, 'Burundi'),
(32, 'Chile'),
(33, 'Chiny'),
(34, 'Chorwacja'),
(35, 'Cypr'),
(36, 'Czad'),
(37, 'Czarnogóra'),
(38, 'Czechy'),
(39, 'Dania'),
(40, 'Demokratyczna Republika Konga'),
(41, 'Dominika'),
(42, 'Dominikana'),
(43, 'Dżibuti'),
(44, 'Egipt'),
(45, 'Ekwador'),
(46, 'Erytrea'),
(47, 'Estonia'),
(48, 'Etiopia'),
(49, 'Fidżi'),
(50, 'Filipiny'),
(51, 'Finlandia'),
(52, 'Francja'),
(53, 'Gabon'),
(54, 'Gambia'),
(55, 'Ghana'),
(56, 'Grecja'),
(57, 'Grenada'),
(58, 'Gruzja'),
(59, 'Gujana'),
(60, 'Gwatemala'),
(61, 'Gwinea'),
(62, 'Gwinea Bissau'),
(63, 'Gwinea Równikowa'),
(64, 'Haiti'),
(65, 'Hiszpania'),
(66, 'Holandia'),
(67, 'Honduras'),
(68, 'Indie'),
(69, 'Indonezja'),
(70, 'Irak'),
(71, 'Iran'),
(72, 'Irlandia'),
(73, 'Islandia'),
(74, 'Izrael'),
(75, 'Jamajka'),
(76, 'Japonia'),
(77, 'Jemen'),
(78, 'Jordania'),
(79, 'Kambodża'),
(80, 'Kamerun'),
(81, 'Kanada'),
(82, 'Katar'),
(83, 'Kazachstan'),
(84, 'Kenia'),
(85, 'Kirgistan'),
(86, 'Kiribati'),
(87, 'Kolumbia'),
(88, 'Komory'),
(89, 'Kongo'),
(90, 'Korea Południowa'),
(91, 'Korea Północna'),
(92, 'Kostaryka'),
(93, 'Kuba'),
(94, 'Kuwejt'),
(95, 'Laos'),
(96, 'Lesotho'),
(97, 'Liban'),
(98, 'Liberia'),
(99, 'Libia'),
(100, 'Liechtenstein'),
(101, 'Litwa'),
(102, 'Luksemburg'),
(103, 'Łotwa'),
(104, 'Macedonia'),
(105, 'Madagaskar'),
(106, 'Malawi'),
(107, 'Malediwy'),
(108, 'Malezja'),
(109, 'Mali'),
(110, 'Malta'),
(111, 'Maroko'),
(112, 'Mauretania'),
(113, 'Mauritius'),
(114, 'Meksyk'),
(115, 'Mikronezja'),
(116, 'Mołdawia'),
(117, 'Monako'),
(118, 'Mongolia'),
(119, 'Mozambik'),
(120, 'Namibia'),
(121, 'Nauru'),
(122, 'Nepal'),
(123, 'Niemcy'),
(124, 'Niger'),
(125, 'Nigeria'),
(126, 'Nikaragua'),
(127, 'Norwegia'),
(128, 'Nowa Zelandia'),
(129, 'Oman'),
(130, 'Pakistan'),
(131, 'Palau'),
(132, 'Panama'),
(133, 'Papua-Nowa Gwinea'),
(134, 'Paragwaj'),
(135, 'Peru'),
(136, 'Polska'),
(137, 'Portugalia'),
(138, 'Republika Południowej Afryki'),
(139, 'Republika Środkowoafrykańska'),
(140, 'Republika Zielonego Przylądka'),
(141, 'Rosja'),
(142, 'Rumunia'),
(143, 'Rwanda'),
(144, 'Saint Kitts i Nevis'),
(145, 'Saint Lucia'),
(146, 'Saint Vincent i Grenadyny'),
(147, 'Salwador'),
(148, 'Samoa'),
(149, 'San Marino'),
(150, 'Senegal'),
(151, 'Serbia'),
(152, 'Seszele'),
(153, 'Sierra Leone'),
(154, 'Singapur'),
(155, 'Słowacja'),
(156, 'Słowenia'),
(157, 'Somalia'),
(158, 'Sri Lanka'),
(159, 'Stany Zjednoczone'),
(160, 'Suazi'),
(161, 'Sudan'),
(162, 'Surinam'),
(163, 'Syria'),
(164, 'Szwajcaria'),
(165, 'Szwecja'),
(166, 'Tadżykistan'),
(167, 'Tajlandia'),
(168, 'Tanzania'),
(169, 'Timor Wschodni'),
(170, 'Togo'),
(171, 'Tonga'),
(172, 'Trynidad i Tobago'),
(173, 'Tunezja'),
(174, 'Turcja'),
(175, 'Turkmenistan'),
(176, 'Tuvalu'),
(177, 'Uganda'),
(178, 'Ukraina'),
(179, 'Urugwaj'),
(180, 'Uzbekistan'),
(181, 'Vanuatu'),
(182, 'Watykan'),
(183, 'Wenezuela'),
(184, 'Węgry'),
(185, 'Wielka Brytania'),
(186, 'Wietnam'),
(187, 'Włochy'),
(188, 'Wybrzeże Kości Słoniowej'),
(189, 'Wyspy Marshalla'),
(190, 'Wyspy Salomona'),
(191, 'Wyspy Świętego Tomasza i Książęca'),
(192, 'Zambia'),
(193, 'Zimbabwe'),
(194, 'Zjednoczone Emiraty Arabskie'),
(195, 'Czeczenia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `places`
--

CREATE TABLE IF NOT EXISTS `places` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `author` varchar(244) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=13 ;

--
-- Zrzut danych tabeli `places`
--

INSERT INTO `places` (`id`, `name`, `description`, `city`, `created`, `modified`, `country_id`, `image`, `author`) VALUES
(2, 'Rynek główny', 'Fajne miejsce, nawet duże i w ogóle kluby i gołębie!', 'Kraków', '2015-01-10 19:39:41', '2015-01-10 19:39:41', 136, 'http://i.wp.pl/a/f/jpeg/32540/krakow-rynek_glowny.jpeg', 'test'),
(4, 'Plaża', 'Słońce, plaża, palmy, wczasy.', 'Copacabana', '2015-01-10 23:46:47', '2015-01-10 23:46:47', 27, 'http://cdn.travelplanet.pl/BZKWRM/TRAD/brazylia/rio-de-janeiro/rio-de-janeiro/mirasol-copacabana_5.jpg', 'alinka'),
(5, 'Miasto Bydgoszcz', 'Najwspanialsze miejsce na świecie', 'Bydgoszcz', '2015-01-10 23:53:49', '2015-01-10 23:55:08', 136, 'http://culture.pl/sites/default/files/images/static/uuid/3694c1c7-8c4c-463c-acbf-f5a2625d46a1_10162.jpg', 'rudy102'),
(6, 'Zapadła Dziura', 'Co tu dużo mówić- zapadła dziura, zabita dechami', 'Toruń', '2015-01-10 23:58:50', '2015-01-10 23:58:50', 2, 'http://www.obmawiamy.pl/wp-content/uploads/2010/06/dziury-w-ziemi.jpg', 'rudy102'),
(7, 'Dom Mateusza', 'Niezwykle ciepłe miejsce, komputerowy SOR, zawsze świeże ciasta, mnóstwo anegdot ze świata pielęgniarek, kochany czarny mały pies, miejsce, gdzie zawsze jestem mile widziana (a przynajmniej nie odczuwam, że jest inaczej). Można się tu napić dobrego alkoholu, oglądnąć dobry film lub pograć w niekończące się partie podczas gier karcianych oraz Scrabbli.\r\nPrzybywajcie turyści z całego świata!', 'Dąbrowa', '2015-01-10 23:58:56', '2015-01-10 23:58:56', 136, 'brak', 'youpendi'),
(8, 'Lemingrad', 'Największe, naturalne, siedlisko lemingów w Polsce. Uwaga! Pod ochroną!', 'Warszawa', '2015-01-11 00:02:34', '2015-01-11 00:02:34', 136, 'http://www.miasteczko-wilanow.pl/frontend/flash/images/4b.jpg', 'rudy102'),
(9, 'Karłowicza 12/24', 'Wolne Mieszkanie na Karłowicza. Siedziba pierwszej Ambasady Bydgoszczy w kraju. Miejsce niezapomnianych i zapomnianych spotkań.', 'Kraków', '2015-01-11 00:07:24', '2015-01-11 00:07:24', 136, 'https://dl-web.dropbox.com/get/zdj%C4%99cia/ii%20sezon/iii%20semestr/I%20otwarcie%20sezonu%20na%20Kar%C5%82owicza%2010.10.2013/skanowanie0036.jpg?_subject_uid=251651350&w=AABz7n2k-sqMKIZsAhtS1orhkc7zF-zmVYNbQJbscmts3Q', 'rudy102'),
(10, 'Przełęcz Legionów', 'to tutaj, dzielni legioniści przyszłej II Brygady Legionów Polskich (a nie Piłsudskiego) zbudowali w niezwykle krótkim czasie siedmiokilometrową drogę, dzięki której wojska austriackie mogły wykonać strategiczne przemieszczenie. Spowodowało to zatrzymanie rosyjskiej ofensywy.\r\nNiech żyją Legioniści! Niech żyje Jan Słuszkiewicz z Bochni (ojczyzny Jarka z Bośni)! Niech żyją!', 'Gorgany', '2015-01-11 00:24:57', '2015-01-11 00:24:57', 178, 'https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-xpa1/v/t1.0-9/10891841_892723237444805_3500413102603536346_n.png?oh=89b01520188fe2bc628b2163e419e2bd&oe=55274D89&__gda__=1429700145_527bece20c7a535bb42897e9b56e3e5e', 'youpendi'),
(11, 'Lokomotywa AGH', 'fajna lokomotywa, chroni przed wiatrem przy paleniu fajek!', 'Kraków', '2015-01-11 00:44:05', '2015-01-11 00:44:05', 136, 'http://idacdonikad2.blox.pl/resource/LokomotywaAGH.jpg', 'mathjas'),
(12, 'zzz', 'zzz', 'zzz', '2015-01-19 01:26:57', '2015-01-19 01:26:57', 2, '', 'testowyuser');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` int(11) NOT NULL,
  `body` text COLLATE utf8_polish_ci NOT NULL,
  `username` text COLLATE utf8_polish_ci NOT NULL,
  `created` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=5 ;

--
-- Zrzut danych tabeli `posts`
--

INSERT INTO `posts` (`id`, `body`, `username`, `created`, `user_id`) VALUES
(3, 'Udało się zalogować', 'Super', '2014-12-06 03:02:52', NULL),
(4, 'Test z id', 'User', '2014-12-06 03:13:02', 12);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
`id` int(11) NOT NULL,
  `body` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(150) CHARACTER SET latin7 NOT NULL,
  `password` varchar(255) CHARACTER SET latin7 COLLATE latin7_general_cs NOT NULL,
  `email` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `role` enum('admin','regular','','') COLLATE utf8_polish_ci NOT NULL DEFAULT 'regular'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=24 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `name`, `role`) VALUES
(12, 'super', '$2a$10$wnnIYK/xp8i4PoT//EcS2u70gLs4FwCyzw4a/hhgqD39UA20wa2R2', 'mr.rajca@gmail.com', 'Mateusz Rajca', 'admin'),
(15, 'alinka', '$2a$10$etHRCEl16MPPGJnsCp9NhOExlRRblCcN6.HiFKsY0ruZMuNtJyZp2', 'alinka@al.pl', 'Alina Rajca', 'regular'),
(17, 'Joe', '$2a$10$YBUSVsiugnTaI91Xy1abQeNkXRr8h8.rVTjUy9QEPJTlHaShFuvk.', 'joanna.prawda@gmail.com', 'Joanna', 'regular'),
(18, 'test', '$2a$10$gxc9xp.k903LA/erPKjy6OkGey.R6CQDMxihqLpxnrLq97bwQfeNO', 'test@test.pl', 'Test Testowicz', 'regular'),
(19, 'dawidziu', '$2a$10$gAAv1r4/zNCJ7duNyix9o.HDOciXO82E3GSK3OihfPdCEQKCrWpri', 'dawid.rzeznik@gmail.com', 'dawid', 'regular'),
(20, 'rudy102', '$2a$10$ZPGIN34QLSL8sVi81wkdLeE/6qNCGzH/ueQaNvxfRbUTe.4/.2O3S', 'msionkowski@o2.pl', 'Maciej sionkowski', 'regular'),
(21, 'youpendi', '$2a$10$YW.VxqUBGKoQDj2hyNgFm.Z7VvEXpWYQW57dtjEUHlHU2oArQ75NS', 'daria.wlodek@gmail.com', 'Daria W', 'regular'),
(22, 'mathjas', '$2a$10$H.RLhBB1m9tFpwRzAYcviON902w1Qv5h2XZjIF60etR8.PfSEAxuG', 'mr.rajca@gmail.com', 'Mateusz Rajca', 'regular'),
(23, 'testowyuser', '$2a$10$uOdbxBLmhqQELz9i8x82mOfUzHMG/jAZCBqI26LCmh.okbxqLoGOO', 'alinka@al.pl', 'adas', 'regular');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `attractions`
--
ALTER TABLE `attractions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `attractions`
--
ALTER TABLE `attractions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `cities`
--
ALTER TABLE `cities`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `comments`
--
ALTER TABLE `comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT dla tabeli `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=196;
--
-- AUTO_INCREMENT dla tabeli `places`
--
ALTER TABLE `places`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `posts`
--
ALTER TABLE `posts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `tests`
--
ALTER TABLE `tests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
